module.exports = {
  context: __dirname + "/src",

    entry: {
        javascript: './main.jsx',
        html: './index.html'
    },

  output: {
    filename: "main.js",
    path: __dirname + "/dist"
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  eslint: {
    failOnWarning: false,
    failOnError: false
  },

  module: {
    preLoaders: [
      {
        test: /\.jsx?$/, 
        loader: 'eslint', 
        exclude: /node_modules/
      }
    ],
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ["babel-loader"]
      },
      {
        test: /\.html$/,
        loader: "file?name=[name].[ext]"
      }
    ]
  }
};
