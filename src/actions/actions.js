export const ADD_USER = 'ADD_USER';
export const ADD_USER_SUCEEDED = 'ADD_USER_SUCEEDED';
export const ADD_USER_FAILED = 'ADD_USER_FAILED';

export const DELETE_USER = 'DELETE_USER';
export const DELETE_USER_SUCCEEDED = 'DELETE_USER_SUCEEDED';
export const DELETE_USER_FAILED = 'DELETE_USER_FAILED';

export const CHANGE_USERNAME = 'CHANGE_USERNAME';
export const CHANGE_USERNAME_SUCCEEDED = 'CHANGE_USERNAME';
export const CHANGE_USERNAME_FAILED = 'CHANGE_USERNAME';

export const CHANGE_NAME = 'CHANGE_NAME';
export const CHANGE_NAME_SUCCEEDED = 'CHANGE_NAME_SUCEEDED';
export const CHANGE_NAME_FAILED = 'CHANGE_NAME_FAILED';


export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCEEDED = 'CHANGE_PASSWORD_SUCEEDED';
export const CHANGE_PASSWORD_FAILED = 'CHANGE_PASSWORD_FAILED';

export const EDIT_NAME = 'EDIT_NAME';
export const EDIT_USERNAME = 'EDIT_USERNAME';
export const EDIT_PASSWORD = 'EDIT_PASSWORD';

export const CANCEL_EDIT = 'CANCEL_EDIT';

export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_USERS_SUCCEEDED = 'FETCH_USERS_SUCCEEDED';
export const FETCH_USERS_FAILED = 'FETCH_USERS_FAILED';

export const LOGIN = 'LOGIN';
export const LOGIN_SUCCEEDED = 'LOGIN_SUCCEEDED';
export const LOGIN_FAILED = 'LOGIN_FAILED';

export const addUser = (name, username, password) => ({ type: ADD_USER, name, username, password });
export const addingUserSuceeded = () => ({type: ADD_USER_SUCEEDED});
export const addingUserFailed = (err) => ({type: ADD_USER_FAILED, err});

export const fetchUsers = () => ({type: FETCH_USERS});
export const userFetchingSuceeded = (users) => ({type: FETCH_USERS_SUCCEEDED, users});
export const userFetchingFailed =  (err) => ({type: FETCH_USERS_FAILED, err});

export const editName = (id) => ({type: EDIT_NAME, id});
export const editUsername = (id) => ({type: EDIT_USERNAME, id});
export const editPassword = (id) => ({type: EDIT_PASSWORD, id});
export const cancelEdit = () => ({type: CANCEL_EDIT});

export const deleteUser = (id) => ({type: DELETE_USER, id});
export const deletingUserSucceeded = () => ({type: DELETE_USER_SUCCEEDED});
export const deletingUserFailed = (err) => ({type: DELETE_USER_FAILED, err});

export const changeUsername = (id, username) => ({type: CHANGE_USERNAME, id, username});
export const changingUsernameSucceeded = () => ({type: CHANGE_USERNAME_SUCCEEDED});
export const changingUsernameFailed = () => ({type: CHANGE_USERNAME_FAILED});

export const changeName = (id, name) => ({type: CHANGE_NAME, id, name});
export const changingNameSuceeded = () => ({type: CHANGE_NAME_SUCCEEDED});
export const changingNameFailed = (err) => ({type: CHANGE_NAME_FAILED, err});

export const changePassword = (id, password) => ({type: CHANGE_PASSWORD, id, password});
export const changingPasswordSucceeded = () => ({type: CHANGE_PASSWORD_SUCCEEDED});
export const changingPasswordFailed = (err) => ({type: CHANGE_PASSWORD_FAILED, err});

export const login = (username, password) => ({type: LOGIN, username, password});
export const loginSucceeded = (username, password) => ({type: LOGIN_SUCCEEDED, username, password});
export const loginFailed = (err) => ({type: LOGIN_FAILED, err});
