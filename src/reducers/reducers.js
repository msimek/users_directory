import { Map, List, OrderedSet } from 'immutable';
import {
  ADD_USER,
  EDIT_NAME,
  CANCEL_EDIT,
  EDIT_USERNAME,
  EDIT_PASSWORD,
  DELETE_USER,
  FETCH_USERS_SUCCEEDED,
  ADD_USER_SUCEEDED,
  DELETE_USER_SUCCEEDED,
  CHANGE_NAME_SUCCEEDED,
  CHANGE_USERNAME_SUCCEEDED,
  CHANGE_PASSWORD_SUCCEEDED,
  ADD_USER_FAILED,
  DELETE_USER_FAILED,
  CHANGE_USERNAME_FAILED,
  CHANGE_NAME_FAILED,
  CHANGE_PASSWORD_FAILED,
  FETCH_USERS_FAILED,
  LOGIN
} from '../actions/actions';

export const initialState = Map({
  editedField: null,
  users: List(),
  errors: OrderedSet(),
  loggedUser: null
});

export const usersApp = (state = initialState, action) => {
  switch (action.type)  {

    case FETCH_USERS_SUCCEEDED:
        return state.merge(Map({
          users: action.users,
          errors: OrderedSet()
        }));
        break;

    case FETCH_USERS_FAILED:
    case ADD_USER_FAILED:
    case DELETE_USER_FAILED:
    case CHANGE_NAME_FAILED:
    case CHANGE_PASSWORD_FAILED:
    case CHANGE_USERNAME_FAILED:
      return state.updateIn(['errors'], s => s.add(action.err));
      break;

    case EDIT_NAME:
      if (state.get('editedField') === null) {
        const newEditedField = Map({editedFieldId: action.id, editedFieldType: 'name'});
        return state.merge(Map({editedField: newEditedField}));
      } else { return state; }
      break;

    case EDIT_USERNAME:
      if (state.get('editedField') === null) {
        return state.setIn(['editedField'], Map({
            editedFieldId: action.id,
            editedFieldType: 'username'
        }));
      } else { return state; }
    break;

  case LOGIN:
    return state.setIn(['loggedUser'], Map({username: action.username,
                                            password: action.password}));
    break;

  case EDIT_PASSWORD:
    if (state.get('editedField') != null) {
      return state.setIn(['editedField'], Map({
        editedFieldId: action.id,
        editedFieldType: 'password'}));
    } else {
      return state;
    }
    break;

  case CANCEL_EDIT:
    return state.setIn(['editedField'], null);
    break;

  default:
    return state;
  }
};
