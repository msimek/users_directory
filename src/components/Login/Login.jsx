import React from 'react';
import { connect } from 'react-redux';
import { login } from '../../actions/actions';

const Login = ({ dispatch }) => {
    let username;
    let password;

    return (
    <div>
    <form onSubmit={ (e) => {
        e.preventDefault();
        dispatch(login(username.value, password.value));
        username.value = '';
        password.value = '';
        }}
    >
        <p>username</p>
        <input type="text" ref={node => {
            username = node
            }}/>

        <p>password</p>
        <input type="password" ref={node => {
            password = node
            }} />
        <br />
        <button type="submit">login</button>
    </form>
    </div>
    );
};

export default connect()(Login);
