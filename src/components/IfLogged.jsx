import React from 'react';
import UserManagement from './UserManagement';
import { connect } from 'react-redux';
import Login from './Login/Login';


const mapStateToProps = (state) => {
  return {
    loggedUser: state.get('loggedUser')
  }
};

const IfLogged = ({ loggedUser }) => (
    loggedUser == null ?
    <Login /> :
    <UserManagement />
);

export default connect(mapStateToProps)(IfLogged);
