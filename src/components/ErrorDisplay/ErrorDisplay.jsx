import React from 'react';
import { Alert } from 'react-bootstrap';
import IPropTypes from 'immutable-props';

const ErrorDisplay = ({ errors }) => (
    errors.size > 0 ?
      <Alert bsStyle="danger">
          {errors.map(err => (<p>{err}</p>))}
      </Alert> :
      null
);


ErrorDisplay.propTypes = {
    errors: IPropTypes.OrderedSet,
}

export default ErrorDisplay;
