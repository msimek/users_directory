import React from 'react';

const NonEditableCell = ({ type, value, onClickHandler }) => (
    type === 'password' ?
    <input type='password' value={value} onClick={onClickHandler} readonly/> :
    <div
        onClick={onClickHandler}
        role="button"
    >
      {value}
    </div>
);

NonEditableCell.propTypes = {
    value: React.PropTypes.oneOfType([React.PropTypes.number,
    React.PropTypes.string]),
    onClickHandler: React.PropTypes.func,
    type: React.PropTypes.string,
};

export default NonEditableCell;

