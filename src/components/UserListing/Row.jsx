import React from 'react';
import Cell from './Cell';

import { connect } from 'react-redux';
import { editName, editUsername,
         editPassword, changePassword,
         cancelEdit, deleteUser,
         changeName, changeUsername,
        } from '../../actions/actions';

let Row = ({ dispatch, userId, userName, userUsername, editedField, userPassword }) => {
    let isNameEditable = false;
    let isUsernameEditable = false;
    let isPasswordEditable = false;

    if (editedField) {
        const editedFieldId = editedField.get('editedFieldId');
        const editedFieldType = editedField.get('editedFieldType');

        if (editedFieldId === userId) {
            if (editedFieldType === 'username') { isUsernameEditable = true; }
            if (editedFieldType === 'name') { isNameEditable = true; }
            if (editedFieldType === 'password') { isPasswordEditable = true;}
        }
    }

    const onClickCancel = () => dispatch(cancelEdit());

    return (
            <tr>
                <th><Cell isEditable={false} value={userId} onClickHandler={() => {}}/></th>

                <th><Cell isEditable={isNameEditable}
                          value={userName}
                          onClickHandler={() => dispatch(editName(userId))}

                          onClickCancel={onClickCancel}

                          onClickSave={(state) => dispatch(changeName(userId, state.val))}
                    />
                </th>

                <th>
                    <Cell isEditable={isUsernameEditable}
                          value={userUsername}
                          onClickHandler={() => dispatch(editUsername(userId))}
                          onClickCancel={onClickCancel}

                           onClickSave={(state) => dispatch(changeUsername(userId, state.val))}
                    />
                </th>

                <th>

                    <Cell isEditable={isPasswordEditable}
                          value={userPassword}
                          type='password'
                          onClickHandler={() => dispatch(editPassword(userId))}
                          onClickCancel={onClickCancel}

                           onClickSave={(state) => dispatch(changePassword(userId, state.val))}
                    />
                </th>

                <th>
                    <button type="button" onClick={() => dispatch(deleteUser(userId))}>delete</button>
                </th>
            </tr>
        )
    };

Row =  connect()(Row);

export default Row;
