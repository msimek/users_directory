import React from 'react';
import Header from './Header';
import Row from "./Row";

export const UserListing = props => {
    const rows = props.userData.map((user) => <Row editedField={props.editedField} userId={user.get('id')} userName={user.get('name')} userUsername={user.get('username')} userPassword={user.get('password')}/> );
        return (
            <div>
                <table>
                    <Header />
                    {rows}
                </table>
            </div>
        )
    };

export default UserListing;



