import React from 'react';
import EditableCell from './EditableCell';
import NonEditableCell from './NonEditableCell';

const Cell = ({value, type, onClickHandler, onClickCancel, onClickSave, isEditable, dispatch}) => (
    isEditable ?
    <EditableCell
        value={value}
        onClickCancel={onClickCancel}
        onClickSave={onClickSave}
        dispatch={dispatch}
    /> :
    <NonEditableCell
        type={type}
        value={value}
        onClickHandler={onClickHandler}
    />
);

Cell.propTypes = {
  value: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number]),
  onClickSave: React.PropTypes.func,
  onClickCancel: React.PropTypes.func,
  onClickHandler: React.PropTypes.func,
  isEditable: React.PropTypes.bool,
  dispatch: React.PropTypes.func,
  type: React.PropTypes.string,
};

export default Cell;
