import React from 'react';

class EditableCell extends React.Component {
  render() {
    return (
      <div>
        <form>
          <input
            type="text"
            defaultValue={this.props.value}
            onChange={e => this.setState({ val: e.target.value })}
          />
          <button
            onClick={() => this.props.onClickSave(this.state)}
          >
            save
          </button>
          <button
            onClick={this.props.onClickCancel}
          >
            cancel
          </button>
        </form>
      </div>
    );
  }
}

EditableCell.propTypes = {
  value: React.PropTypes.string,
  onClickSave: React.PropTypes.func,
  onClickCancel: React.PropTypes.func,
};

export default EditableCell;
