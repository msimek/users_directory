import React from 'react';
import UserListing from './UserListing/UserListing'
import NewUser from './NewUser'
import ErrorDisplay from './ErrorDisplay/ErrorDisplay';
import { connect } from 'react-redux'
import Immutable from 'immutable';
import IPropTypes from 'immutable-props';
import { Jumbotron } from 'react-bootstrap';

const mapStateToProps = (state) => {
  return {
    editedField: state.get('editedField'),
    users: state.get('users'),
    errors: state.get('errors'),
  }
};

const UserManagement = ({users, editedField, errors}) => (
  <Jumbotron>
    <ErrorDisplay errors={errors} />
    <UserListing
        userData={users}
        editedField={editedField}
    />
    <NewUser />
  </Jumbotron>
);

UserManagement.propsTypes = {
    users: IPropTypes.List,
    editedField: IPropTypes.Map,
    errors: IPropTypes.List,
}

export default connect(mapStateToProps)(UserManagement);
