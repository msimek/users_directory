import React from 'react';
import { connect } from 'react-redux';
import { addUser } from '../actions/actions';

const NewUser = ({ dispatch }) => {
    let name;
    let username;
    let password;

    return (
    <div>
    <form onSubmit={ (e) => {
        e.preventDefault();
        dispatch(addUser(name.value, username.value, password.value));
        name.value = '';
        username.value = '';
        password.value = '';
    }
    }>
        <p>name:</p>
        <input type="text" ref={node => {
            name = node
            }} />

        <p>username</p>
        <input type="text" ref={node => {
            username = node
            }}/>

        <p>password</p>
        <input type="password" ref={node => {
            password = node
            }} />
        <br />
        <button type="submit">add user</button>
    </form>
    </div>
    );
};

export default connect()(NewUser);
