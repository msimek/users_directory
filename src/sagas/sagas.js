import {
    take,
    put,
    call,
    fork,
    select
} from 'redux-saga/effects';

import Immutable from 'immutable';
import base64 from 'base-64';

import {
    FETCH_USERS,
    ADD_USER,
    DELETE_USER,
    CHANGE_NAME,
    CHANGE_USERNAME,
  CHANGE_PASSWORD,
    userFetchingSuceeded,
    userFetchingFailed,
    addingUserSuceeded,
    addingUserFailed,
    fetchUsers,
    deletingUserFailed,
    deletingUserSucceeded,
    changingNameSuceeded,
    changingUsernameFailed,
    changingUsernameSucceeded,
    changingNameFailed,
    changePassword,
    changingPasswordFailed,
    changingPasswordSucceeded
}
from '../actions/actions';

const USER_API_BASE = 'http://localhost:8090/users';
const USER_SECURED_API_BASE = 'http://localhost:8090/users/secured';

export function* rootSaga() {
    yield [
      fork(fetchUsersSaga),
      fork(addUserSaga),
      fork(deleteUserSaga),
      fork(changeUsernameSaga),
      fork(changeNameSaga),
      fork(changePasswordSaga)
    ];
}

function* fetchUsersSaga() {
    while (true) {
        yield take(FETCH_USERS);
        try {
            const res = yield call(fetch, USER_API_BASE);
            const users = yield res.json();
            yield put(userFetchingSuceeded(Immutable.fromJS(users)));
        }
        catch(err) {
            yield put(userFetchingFailed(err));
        }

    }
}

function* addUserSaga() {
    while (true) {
        const {name, username, password} = yield take(ADD_USER);

        try {
            yield call(fetch, USER_API_BASE,
                       { headers: {"Content-Type": "application/json" },
                         method: 'POST',
                         body: JSON.stringify({name, username, password})
                       });
            yield put(addingUserSuceeded());
            yield put(fetchUsers());
        }
        catch(err) {
            yield put(addingUserFailed(err));
        }

    }
}

function* changeUsernameSaga() {
    

    while (true) {
      const { username, id } = yield take(CHANGE_USERNAME);
      const URL = `${USER_API_BASE}/${id}`;

        try {
            yield call(fetch, URL, { headers: {"Content-Type": "application/json" },
                                     method: 'PATCH',
                                     body: JSON.stringify({username})
                                   });
            yield put(changingUsernameSucceeded);
            yield put(fetchUsers());
        }
        catch(err) {
            console.log(err);
            yield put(changingUsernameFailed(err));
        }
    }
}

function* changeNameSaga() {
    while (true) {
        const { name, id } = yield take(CHANGE_NAME);
        const URL = `${USER_API_BASE}/${id}`;
        try {
            yield call(fetch, URL, { headers: {"Content-Type": "application/json" },
                                     method: 'PATCH',
                                     body: JSON.stringify({name})
                                   });
            yield put(changingNameSuceeded());
            yield put(fetchUsers());
        }
        catch(err) {
            console.log(err);
            yield put(changingNameFailed(err));
        }
    }
}

function* changePasswordSaga() {
  while (true) {
    const { password, id } = yield take(CHANGE_PASSWORD);
    const URL = `${USER_API_BASE}/${id}`;
    try {
      yield call(fetch, URL, { headers: {"Content-Type": "application/json" },
                               method: 'PATCH',
                               body: JSON.stringify({password})
                             });
      yield put(changingPasswordSuceeded());
      yield put(fetchUsers());
    }
    catch(err) {
      console.log(err);
      yield put(changingPasswordFailed(err));
    }
  }
}



function* deleteUserSaga() {
    while (true) {
        const {id} = yield take(DELETE_USER); 
        try {
          const loggedUser = yield select((state) => state.get('loggedUser'));
          const username = loggedUser.get('username');
          const password = loggedUser.get('password');

          const bauth = base64.encode(`${username}:${password}`);
          const credentials = `Basic ${bauth}`;
          const URL = `${USER_SECURED_API_BASE}/${id}`;

          const res = yield call(fetch, URL, {
            method: 'DELETE',
            headers: {
              "Authorization": credentials
            }
          });

          if (res.status >= 200 && res.status <= 300) {
            yield put(deletingUserSucceeded());
            yield put(fetchUsers());
          } else {
            throw res.status;
          }
        }
        catch(err) {
          console.log(err);
          let errMessage = "Unknown Error";
          if (err === 500) {
            errMessage = "User cannot be deleted."
          };
          if (err === 401) {
            errMessage = "Authentication Error."
          };

            yield put(deletingUserFailed(errMessage));
        }

    }
}

export default rootSaga;
