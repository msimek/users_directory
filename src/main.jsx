import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga'
import { usersApp, initialState } from './reducers/reducers';
import { rootSaga } from './sagas/sagas';
import { fetchUsers } from './actions/actions';
import UserManagement from './components/UserManagement';
import IfLogged from './components/IfLogged'


const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(usersApp, initialState, composeEnhancers(applyMiddleware(sagaMiddleware)));

const render = () => {
    ReactDOM.render(
        <Provider store={store}>
        <IfLogged />
    </Provider>,
    document.getElementById('root'));
};

sagaMiddleware.run(rootSaga);

store.dispatch(fetchUsers());
store.subscribe(render);
